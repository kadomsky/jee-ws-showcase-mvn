package ws.converter;


import java.math.BigDecimal;

public class ConverterClient {
  public static void main(String[] argv) {
    ws.converter.ExchangeRatesSoap service = new ExchangeRates().getExchangeRatesSoap();
    //invoke business method
    BigDecimal result = service.getCurrentExchangeRate("BS", "USD", 1);
    System.out.println("Conversion result: " + result);
  }
}
