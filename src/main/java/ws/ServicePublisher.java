package ws;

import ws.hello.HelloServiceImpl;

/**
 * Created by Cyril on 2020-03-22.
 */
public class ServicePublisher {

    public static void main(String... args) {
        javax.xml.ws.Endpoint.publish("http://localhost:1900/ws/hello", new HelloServiceImpl());
    }
}
