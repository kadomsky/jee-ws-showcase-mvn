package ws.hello;

import javax.jws.WebMethod;
import javax.jws.WebService;


@WebService(serviceName = "HelloService", portName = "HelloPort",
        targetNamespace = "http://ws.JSS.javajoy.net/DemoWS",
        endpointInterface = "ws.hello.HelloService")
public class HelloServiceImpl implements HelloService {

    public String sayHelloWorldFrom( String from ) {
        String result = "Hello world, from " + from;
        System.out.println(result);
        return result;
    }

    @WebMethod(exclude = true)
    public int getID() {
        return 0;
    }
}
