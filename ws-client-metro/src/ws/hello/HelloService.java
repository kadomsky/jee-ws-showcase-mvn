package ws.hello;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name="HelloService", targetNamespace = "http://JSS.javajoy.net/DemoWS" )
@SOAPBinding(style= SOAPBinding.Style.DOCUMENT)
public interface HelloService {
    @WebResult(name="helloString")
    @WebMethod(operationName = "sayHello")
    String sayHelloWorldFrom(@WebParam(name = "from", mode = WebParam.Mode.IN) String from);
}