package countries;public class CountryClient {
  public static void main(String[] argv) {
    CountryInfoServiceSoapType service = new CountryInfoService().getCountryInfoServiceSoap();
//invoke business method
    ArrayOftCountryCodeAndName list = service.listOfCountryNamesByCode();
    list.getTCountryCodeAndName().forEach(c -> System.out.println(c.getSISOCode() + " = " + c.getSName()));

  }
}
